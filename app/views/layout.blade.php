<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <!-- CSS are placed here -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    {{ HTML::style('/assets/css/bootstrap.css') }}
    {{ HTML::style('/assets/css/bootstrap-theme.css') }}

    {{ HTML::script('/assets/js/bootstrap.min.js') }}
    {{ HTML::script('/assets/js/jquery-1.11.1.min.js') }}

    <title>
        Prueba Crud Bootstrap
    </title>
</head>
<body>


<div class="content ">
    <div class="container principal">
        @yield("content")
    </div>
</div>
</body>
</html>