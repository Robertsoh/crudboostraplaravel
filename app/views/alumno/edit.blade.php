@extends("layout")
@section('content')

    {{ HTML::ul($errors->all()) }}

    {{ Form::model($alumno, array('route' => array('alumno.update', $alumno->id), 'method' => 'PUT')) }}
    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Editar registro
                </div>

                <div class="panel-body">

                    <div class="col-md-2">
                        {{ Form::label("nombre", "Nombres:") }}
                        {{ Form::text("nombre",null,array('required' => true,'class' => 'form-control')) }}
                    </div>

                    <div class="col-md-2">
                        {{ Form::label("apellido", "Apellidos:") }}
                        {{ Form::text("apellido",null,array('required' => true,'class' => 'form-control')) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label("correo", "Email:") }}
                        {{ Form::text("correo",null,array('required' => true,'class' => 'form-control')) }}
                    </div>
                </div>

                <div class="panel-footer alto">
                    {{ Form::submit("Guardar",array('class' => 'btn btn-success pull-right')) }}
                    <a class="btn btn-danger pull-right" style="margin-right:10px" href="{{ URL::to('alumno') }}">Cancelar</a>
                </div>

            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop