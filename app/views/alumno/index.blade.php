@extends("layout")
@section('content')

    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Lista de Alumnos
                    <a href="{{ URL::to('alumno/create')}}" class="pull-right">
                        <i class="glyphicon glyphicon-file" aria-hidden="true"></i> Nuevo
                    </a>

                </div>
                <div class="panel-body">
                    <!-- will be used to show any messages -->
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th class="col-sm-1">ID</th>
                            <th>Nombres</th>
                            <th class="col-sm-3">Apellidos</th>
                            <th class="col-sm-3">Correo</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($alumnos as $alumno)
                            <tr>
                                <td>{{ $alumno->id }}</td>
                                <td>{{ $alumno->nombre }}</td>
                                <td>{{ $alumno->apellido }}</td>
                                <td>{{ $alumno->correo }}</td>
                                <td>
                                    {{ Form::open(array('url' => 'alumno/' . $alumno->id, 'class' => 'pull-right')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    <a class="btn btn-info" style="margin-right:5px"
                                       href="{{ URL::to('alumno/' . $alumno->id . '/edit') }}">
                                        <i class="glyphicon glyphicon-edit"></i>
                                        Editar
                                    </a>
                                    {{ Form::button('<i class="glyphicon glyphicon-trash"></i> Eliminar',
                                        array('type' => 'submit',
                                        'class' => 'btn btn-danger',
                                        'onclick' => 'javascript: if (!confirm("Esta seguro que desea eliminar el registro?")) return false;',
                                        )) }}


                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
