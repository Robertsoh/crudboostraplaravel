@extends("layout")
@section('content')

    {{ HTML::ul($errors->all()) }}

    <div class="container">
        <div class="col-md-8">
            {{ Form::open(array('url' => 'alumno', 'method' => 'post')) }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    CREAR NUEVO ALUMNO
                </div>
                <div class="panel-body">

                    <div class="col-md-2">
                        {{ Form::label("nombres", "Nombres:") }}
                        {{ Form::text("nombres",'',array('required' => true,'class' => 'form-control')) }}
                    </div>

                    <div class="col-md-2">
                        {{ Form::label("apellidos", "Apellidos:") }}
                        {{ Form::text("apellidos",'',array('required' => true,'class' => 'form-control')) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label("email", "Email:") }}
                        {{ Form::text("email",'',array('required' => true,'class' => 'form-control')) }}
                    </div>


                </div>
                <div class="panel-footer alto" >
                    {{ Form::button('<i class="glyphicon glyphicon-trash"></i> Guardar',
                         array( 'type' => 'submit',
                                'class' => 'btn btn-success pull-right',

                        ))
                    }}
                    <a class="btn btn-small btn-danger pull-right" href="{{ URL::to('alumno')}}" style="margin-right:10px">
                        <i class="glyphicon glyphicon-remove"></i>Cancelar
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@stop