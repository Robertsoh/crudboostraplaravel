<?php
/**
 * Created by PhpStorm.
 * User: team
 * Date: 20/01/15
 * Time: 12:25 PM
 */

class AluController extends BaseController{

    public function index()
    {
        //
        $alumnos = Alumno::all();
        return View::make('alumno.index')->with('alumnos', $alumnos);
    }

    public function create(){

        return View::make('alumno.create');
    }


    public function store()
    {
        //
        try{
            $rules = array(
                'nombres'       => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('alumno/create')->withErrors($validator);

            } else {
                // store
                $alumno = new Alumno();
                $tmp = Alumno::where('nombre', '=', Input::get('nombres'))->first();
                print_r($tmp );
                if ($tmp){
                    Session::flash('message', 'Existe un nombre identico, verique nuevamente!');
                }else{
                    $alumno->nombre       = Input::get('nombres');
                    $alumno->apellido       = Input::get('apellidos');
                    $alumno->correo      = Input::get('email');

                    $alumno->save();
                    Session::flash('message', 'Se creo el registro correctamente!');
                }
                return Redirect::to('alumno');
            }
        }catch (Exception $error){
            print "Error: ".$error->getMessage();
        }
    }

    public function edit($id)
    {
        //
        //
        $alumno = Alumno::find($id);

        // show the edit form and pass the nerd
        return View::make('alumno.edit')
            ->with('alumno', $alumno);
    }

    public function update($id)
    {
        //
        try{
            $rules = array(
                'nombre'       => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('alumno/'.$id.'/edit')->withErrors($validator);

            } else {
                // store
                $alumno = Alumno::find($id);
                $alumno->nombre       = Input::get('nombre');
                $alumno->apellido       = Input::get('apellido');
                $alumno->correo      = Input::get('correo');

                $alumno->save();
                Session::flash('message', 'Se Actualizó el registro correctamente!');
                return Redirect::to('alumno');
            }
        }catch (Exception $error){
            print "Error: ".$error->getMessage();
        }
    }

    public function destroy($id)
    {
        try{
            // delete
            $alumno = Alumno::find($id);
            $alumno->delete();
            // redirect
            Session::flash('message', 'Se elimino el registro correctamente!');
            return Redirect::to('alumno');
        }catch (Exception $error){
            print "Error: ".$error->getMessage();
        }

    }


}

